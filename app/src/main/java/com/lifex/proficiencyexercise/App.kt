package com.lifex.proficiencyexercise

import android.app.Application
import android.content.Context

class App : Application(){
    override fun onCreate() {
        super.onCreate()
    }

    companion object{

        private fun getApplication(): Context {
            return App().applicationContext
        }
    }
}