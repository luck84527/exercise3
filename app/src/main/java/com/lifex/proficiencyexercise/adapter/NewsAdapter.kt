package com.lifex.proficiencyexercise.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.lifex.proficiencyexercise.R
import com.lifex.proficiencyexercise.databinding.AdapterNewsBinding
import com.lifex.proficiencyexercise.model.NewsResponse
import com.lifex.proficiencyexercise.model.RowsItem


class NewsAdapter(private val fragment: Context, private val items: ArrayList<RowsItem>) :
    RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.adapter_news, parent, false
            )
        )
    }

    override fun getItemCount(): Int = items.size
    inner class ViewHolder(private val binding: AdapterNewsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: RowsItem) = with(binding) {
            binding.news = item
            binding.invalidateAll()
        }
    }

    fun refresh(refreshList: List<RowsItem>?) {
        items.clear()
        items.addAll(refreshList ?: ArrayList())
        notifyDataSetChanged()
    }
}
