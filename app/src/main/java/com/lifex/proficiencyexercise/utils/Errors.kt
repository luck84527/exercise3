package com.lifex.proficiencyexercise.utils

import android.app.Activity
import android.view.View
import androidx.fragment.app.Fragment
import com.lifex.proficiencyexercise.R
import com.lifex.proficiencyexercise.repo.utils.ApiUtils.getError
import com.lifex.proficiencyexercise.repo.utils.AppError


fun Activity.getError(error: AppError): String {
    return extractError(error)
}

fun View.handleError(error: AppError?,fragment: Fragment) {
    snackBar(fragment.requireActivity().getError(error?: AppError.CommonError))
}
fun View.handleError(error: AppError?,activity: Activity) {
    snackBar(activity.getError(error?: AppError.CommonError))
}

fun Activity.extractError(error: AppError): String {
    when (error) {
        is AppError.NetworkError -> {
            return getString(R.string.networkError)
        }
        is AppError.ApiUnauthorized -> {
            return error.message
        }
        is AppError.ApiError -> {
            return error.message
        }
        is AppError.ApiFailure -> {
            return error.message
        }
       else-> {
            return getString(R.string.somethingWentWrong)
        }
    }
}
