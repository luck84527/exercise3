package com.lifex.proficiencyexercise.repo

import com.lifex.proficiencyexercise.model.RowsItem
import com.lifex.proficiencyexercise.model.base.ApiResponse
import io.reactivex.Observable
import retrofit2.http.GET
import java.util.*

interface Apis {

    companion object {
        private const val GET_NEWS = "/s/2iodh4vg0eortkl/facts.json"
    }

    @GET(GET_NEWS)
    fun getNews(): Observable<ApiResponse<ArrayList<RowsItem>>>


}