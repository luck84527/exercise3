package com.lifex.proficiencyexercise.repo

import androidx.databinding.library.baseAdapters.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient {
    private const val BASE_URL_DEV = "https://dl.dropboxusercontent.com/"
    private const val BASE_URL = BASE_URL_DEV

    private val LOGGING_INTERCEPTOR by lazy {


        HttpLoggingInterceptor().setLevel(
            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        )
    }

    private val OK_HTTP_CLIENT by lazy {
        OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(LOGGING_INTERCEPTOR)
            .addInterceptor(AuthorizationInterceptor())
            .build()
    }

    private val RETROFIT by lazy {
        Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GSON))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    val GSON: Gson by lazy {
        GsonBuilder().create()
    }

    val apis: Apis by lazy { RETROFIT.create(Apis::class.java) }

    private class AuthorizationInterceptor : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {

            /**  We Can check Authorization here*/

            val originalRequest = chain.request()
            return chain.proceed(originalRequest)

        }
    }
}