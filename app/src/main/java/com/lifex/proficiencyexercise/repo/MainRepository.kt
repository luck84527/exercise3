package com.lifex.proficiencyexercise.repo

class MainRepository constructor(private val apis: Apis) {
    fun getNews() = apis.getNews()
}