package com.lifex.proficiencyexercise.ui

import androidx.lifecycle.ViewModel
import com.lifex.proficiencyexercise.model.RowsItem
import com.lifex.proficiencyexercise.repo.MainRepository
import com.lifex.proficiencyexercise.repo.utils.*
import com.lifex.proficiencyexercise.utils.NetworkHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlin.collections.ArrayList

class MainActivityViewModel(
    private val networkHelper: NetworkHelper,
    private val mainRepository: MainRepository
) : ViewModel() {


    val newsResponse by lazy { SingleLiveEvent<Resource<List<RowsItem>>>() }
    private val compositeDisposable = CompositeDisposable()

    init {
        getNews()
    }

    fun getNews() {
        if (!networkHelper.isNetworkConnected()) {
            newsResponse.value = Resource.error(AppError.NetworkError)
            return
        }
        newsResponse.value = Resource.loading()
        val api = mainRepository.getNews().map {
            it.rows
        }

        val disposable = api.subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> handleResults(result) },
                { error -> handleError(error) },
            )

        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun handleResults(list: ArrayList<RowsItem>?) {
        if (list != null) {
            newsResponse.value = Resource.success(list)
        } else {
            newsResponse.value = Resource.error(AppError.CommonError)
        }
    }

    private fun handleError(t: Throwable?) {
        newsResponse.value = Resource.error(t?.failureAppError() ?: AppError.CommonError)
    }


}
