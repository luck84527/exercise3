package com.lifex.proficiencyexercise.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.lifex.proficiencyexercise.R
import com.lifex.proficiencyexercise.adapter.NewsAdapter
import com.lifex.proficiencyexercise.databinding.ActivityMainBinding
import com.lifex.proficiencyexercise.repo.utils.Status
import com.lifex.proficiencyexercise.utils.*


class MainActivity : AppCompatActivity() {


    private lateinit var binding: ActivityMainBinding

    //Variables initialized
    private lateinit var loginViewModel: MainActivityViewModel
    private val adapter = NewsAdapter(this, ArrayList())
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        loginViewModel =
            ViewModelProvider(this, ViewModelFactory(this)).get(MainActivityViewModel::class.java)
        binding.rvNews.adapter = adapter
        binding.viewModel = loginViewModel
        loadData()
    }

    private fun loadData() {
        loginViewModel.newsResponse.observe(this, {
            it?.let { resource ->
                binding.invalidateAll()
                when (resource.status) {
                    Status.SUCCESS -> {
                        adapter.refresh(resource.data)
                    }
                    Status.ERROR -> {
                        binding.parent.handleError(it.error, this)
                    }
                    else -> {
                    }
                }
            }
        })


    }


}
