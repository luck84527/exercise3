package com.lifex.proficiencyexercise.ui

import android.content.Context
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lifex.proficiencyexercise.repo.MainRepository
import com.lifex.proficiencyexercise.repo.RetrofitClient
import com.lifex.proficiencyexercise.utils.NetworkHelper


class ViewModelFactory(private val context: Context) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainActivityViewModel::class.java)) {
            return MainActivityViewModel(
                networkHelper = NetworkHelper(context),
                mainRepository = MainRepository(RetrofitClient.apis)
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}