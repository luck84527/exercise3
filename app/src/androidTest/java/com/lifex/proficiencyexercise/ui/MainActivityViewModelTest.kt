package com.lifex.proficiencyexercise.ui

import android.content.Context
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.lifex.proficiencyexercise.repo.MainRepository
import com.lifex.proficiencyexercise.repo.RetrofitClient
import com.lifex.proficiencyexercise.utils.NetworkHelper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityViewModelTest {
    private lateinit var mainActivityViewModel: MainActivityViewModel
    private val appContext: Context = InstrumentationRegistry.getInstrumentation().targetContext

    @Before
    fun setUp() {
        mainActivityViewModel = MainActivityViewModel(NetworkHelper(appContext), MainRepository(RetrofitClient.apis))

    }

    @Test
    fun testNews() {
        mainActivityViewModel.getNews()

    }

}